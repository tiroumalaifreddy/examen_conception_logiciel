import uvicorn
import requests
import random

from fastapi import FastAPI

app = FastAPI()

url_audiodb = "https://www.theaudiodb.com/api/v1/"
url_lyricsovh = "https://api.lyrics.ovh/v1/"

@app.get("/")
def status():
    status_audiodb = requests.get(url_audiodb).status_code
    status_lyricsovh = requests.get(url_lyricsovh + "Coldplay/Adventure of a Lifetime").status_code
    return {"status_audiodb" : status_audiodb, "status_lyricsovh" : status_lyricsovh}


@app.get("/random/{artist_name}")
async def random_song(artist_name : str):
    artist = requests.get(url_audiodb + "json/2/search.php?s=" + artist_name)
    if artist.json()['artists'] == None:
        return artist.json()
    artist_id = artist.json()['artists'][0]['idArtist']
    albums = requests.get(url_audiodb + "json/2/album.php?i=" + artist_id).json()['album']
    if albums == None:
        return requests.get(url_audiodb + "json/2/album.php?i=" + artist_id).json()
    album_random = random.choice(albums)
    id_album_random = album_random['idAlbum']
    songs = requests.get(url_audiodb + "json/2/track.php?m=" + id_album_random).json()['track']
    while songs == None:
        id_album_random = album_random['idAlbum']
        songs = requests.get(url_audiodb + "json/2/track.php?m=" + id_album_random).json()['track']
    song_random = random.choice(songs)
    song_random_name = song_random['strTrack']
    song_random_link = song_random['strMusicVid'] 
    lyrics = requests.get(url_lyricsovh + artist_name + "/" +song_random_name)
    if lyrics.status_code != 200:
        lyrics_json = {"lyrics" : None}
    else:
        lyrics_json = lyrics.json()
    return {"artist" : artist_name, "title" : song_random_name, "suggested_youtube_url" : song_random_link, "lyrics" : lyrics_json["lyrics"]}

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)