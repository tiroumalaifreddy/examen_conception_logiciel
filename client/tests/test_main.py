from unittest import TestCase

from main import isLyrics, load_rudy_from_json

class Test_isLyrics(TestCase):
    def test_isLyrics(self):
        playlist_example = load_rudy_from_json("example_json/playlist_example.json")
        song = playlist_example[0]
        self.assertTrue(isLyrics(song))

if __name__ == '__main__':
    unittest.main()