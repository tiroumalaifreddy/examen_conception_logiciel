from dotenv import load_dotenv
import os
import requests
import json
import random

load_dotenv()

url_server = os.getenv("SERVER_URL")


def load_rudy_from_json(json_path):
    with open(json_path) as f:
        return json.load(f)

def get_random_song(artiste : str):                                     #Get a random song detail from the webservice
    song = requests.get(url_server + "/random/" + artiste).json()
    return song

def isLyrics(song : dict):                          
    return song['lyrics'] != None and song['lyrics'] != "[Instrumental]" #Some songs don't have lyrics and are marked as "[Instrumental]"

def playlist_generator(rudy_file_path : str, number_of_songs : int = 20):
    artists_file = load_rudy_from_json(rudy_file_path)
    weights = []
    artists_name = []
    for artiste in artists_file:
        weights.append(int(artiste['note']))
        artists_name.append(artiste['artiste'])
    playlist_artists = random.choices(artists_name, weights = weights, k = number_of_songs)
    playlist = []
    for artist_name in playlist_artists:
        song = get_random_song(artist_name)
        count = 0
        max_allowed = 6
        while isLyrics(song) == False:                 # We verify that the selected song has Lyrics; if not we choose an other song
            song = get_random_song(artist_name)        # However, we put a limit on the while loop as some artists only have a few songs and it is possible that those songs don't have lyrics at all
            count += 1
            if count == max_allowed:
                break 
        playlist.append(song)
    return playlist

if __name__ == "__main__":
    print('Enter json file path:')
    rudy_path = input()
    print('Enter number of songs as integer:')
    number_of_songs = int(input())
    playlist = playlist_generator(rudy_path, number_of_songs = number_of_songs)
    with open('playlist.json', 'w') as outfile:
        json.dump(playlist, outfile, indent=4)