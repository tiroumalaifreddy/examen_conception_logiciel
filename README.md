# Karaoke's playlist generator

This application retrieves songs' details in various API and generate a playlist from a specific json file. 
## Architecture

It is composed of two part : a webservice exposing songs' details and a client generating a playlist.

```mermaid
graph TD;
    Client-->|Send playlist request|Webservice;
    Webservice-->|Returns the playlist|Client;
```

## Requirements

Each part of the application has a `requirement.txt` file. To install requirements of a certain part, get into the root of the application part (`webservice/` or `client/`) and run :

`pip install -r requirements.txt`.

### Optionnal 


<details><summary>
Setup a virtual environment is better as it ensures to have the exact versions of each package</summary>
<p>

```bash
python3 -m venv venv
## windows
.\venv\Scripts\activate
## linux
source venv/bin/activate
```
</p>
</details>

## Quickstart

### Webservice

You can launch the webservice from the root with the following commands: 

```bash
cd webservice/
uvicorn --reload main:app
```

There are two endpoints:

- `/` returns the status of the external APIs used
- `/random/{artist_name}` returns a json file with details of a random song from the artist. Replace `{artist_name}` by the name of the artist you wish.

### Client

You can launch the generator with the following command from the root:

`python3 client/main.py`

You may then enter your file's path and the number of songs you wish to have in the playlist. The playlist is exported at the root of the application as `playlist.json`.

## Client's spefications

- The json file you input must look like this:

```json
[{
    "artiste": "daft punk",
    "note": 18
},
{
    "artiste":"gloria gaynor",
    "note": 10
},
{
    "artiste":"boney m",
    "note": 5
},
{
    "artiste":"oasis",
    "note": 20
},
{
    "artiste":"Bob Marley",
    "note": 10
},
{
    "artiste":"Tryo",
    "note": 20
},
{
    "artiste":"Technotronic"
    "note": 10
},
{
    "artiste":"dire straits",
    "note": 16
}]
```
The higher is the note, the more likely it will be in the playlist. This example is also present in `rudy_json` folder and to use it enter the following path when asked: 

`client/example_json/rudy.json`

- You must enter an integer for the number of songs.

- You must have SERVER_URL as an environment variable in your operating system. This variable refers to the adress of the launched webservice (you can find the adress in the shell when the webservice is launched). 

## Export environment variable

There are two methods :

- Easy one : you have to create an `.env` file at the root of the `client` folder and specify the environment variable (here `SERVER_URL={your webservice URL}`).

- Other method : 
    - Linux : run `export SERVER_URL={your webservice URL}`
    - Windows : follow the instructions from this link : https://phoenixnap.com/kb/windows-set-environment-variable

## Unit test

To run the test in the client part, run the following command : 

```bash
cd client/
python3 -m unittest tests.test_main.Test_isLyrics
```